
# ErrorBox

ErrorBox is an .iso file (Ubuntu 20.04) with three levels and it aims to make you self sufficient.

Each of the 3 levels will test your:
- Ability to use Google efficiently.
(for instance, use Google’s [search operators](https://ahrefs.com/blog/google-advanced-search-operators/) to get exactly what you want)
- Ability to sit for, at times, maybe for hours or even days, at a single error and not give up.
- Ability to learn just enough of something in order to build it/pass the level.
(for instance, don’t go take a full-fledged full-stack web application course in Coursera and spend two months just for finishing the level - you could do that but try to think from [the first principles](https://fs.blog/2018/04/first-principles/) and focus on the fundamentals rather than on the syntax or the rules i.e, [learn by doing](https://en.wikipedia.org/wiki/Learning-by-doing))

All the levels are bootstrapped with errors that will start harassing you right from the moment you touch the terminal :)


## Instructions:

After installing the iso file in your virtual machine, 
Go to /home/ where you will see your first level - “Level-1-Django”.

Now, you will need to build the project, fix the errors as you go on, and get to the final page. You need to do the same for the next two levels - build the project, fix the errors, complete it.


**Created by [amFOSS](https://amfoss.in/)**
